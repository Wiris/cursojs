// Dados Primitivos
// strings
const nome = 'João';
const segundoNome = `Vitor`;
const sobrenome = "Silva";

// numbers
const idade = 20;
const peso = 75.5;

// undefined
let altura;

// null
let produto = null;

// boolean
let possuiHobbies = false;
let tenhoFilhos = true;

// função typeof retorna o tipo de um dado
console.log(typeof nome);