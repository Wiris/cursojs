let num1 = 1;
let num2 = 2.5;

console.log(num1.toString() + num2); // imprime o valor convertendo números para string
//num1 = num1.toString(); // guarda na variável o em string
console.log(num1.toString(2)); // imprime em binário
console.log(num1.toFixed(2)); // fixa duas casas decimais
console.log(Number.isInteger(num1)); //verifica se o valor é um inteiro
let temp = num1 * "ola";
console.log(Number.isNaN(temp)); // verifica se a variável é um número

let num3 = 0.8;
let num4 = 0.1;

num3 += num4;
num3 += num4;

console.log(num3);
// Por conta da imprecisão do JS é necessário fazer a conversão dos valores
console.log(Number.parseFloat(num3.toFixed(2)));
console.log(Number.isInteger(num3));