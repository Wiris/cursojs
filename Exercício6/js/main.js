let pessoas = [];

function Cadastrar() {
    let form = document.querySelector("form");
    form.onsubmit = (evento) => {
        evento.preventDefault();
    }

    let nome = document.getElementById("nome").value;
    let sobrenome = document.getElementById("sobrenome").value;
    let peso = document.getElementById("peso").value;
    let altura = document.getElementById("altura").value;
    let texto = document.getElementById("texto");

    let pessoa = {
        nome,
        sobrenome,
        peso,
        altura
    }

    pessoas.push(pessoa);

    document.getElementById("nome").value = "";
    document.getElementById("sobrenome").value = "";
    document.getElementById("peso").value = "";
    document.getElementById("altura").value = "";

    texto.innerHTML += `<p>${nome} ${sobrenome} ${peso} ${altura}</p>`;
    console.log(pessoas);

}