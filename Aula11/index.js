let num1 = 3.658;
let num2 = Math.floor(num1); // arredonda para baixo
let num3 = Math.ceil(num1); // arredonda para cima
let num4 = Math.round(num1); // arredonda automaticamente
let num5 = Math.max(1, 2, 3, 4, 5, 6, 7, 8, 9, 10); // retorna o maior valor da sequencia
let num6 = Math.min(1, 2, 3, 4, 5, 6, 7, 8, 9, 10); // retorna o menor valor da sequencia
let num7 = Math.random(); // gera números aleatórios entre 0 e 1, exclui o 1
let num8 = Math.random() * (10 - 1) + 1; // gera números aleatórios entre 1 e 10, exclui o 10
let num9 = (9 ** 0.5); // forma simplificada de calcular a raiz quadrada


console.log(num2);
console.log(num3);
console.log(num4);
console.log(num5);
console.log(num6);
console.log(num7);
console.log(num8);
console.log(num9);