function saudacao(nome) {
    return "Bom Dia " + nome + " !";
}

const variavel = saudacao("Wiris");

console.log(variavel);

function Soma(x = 1, y = 1) { // funcionam como valores padrão caso nada seja passado
    const resultado = x + y;
    return resultado;
}

console.log(Soma(1, 3));
console.log(Soma(5, 2));
console.log(Soma(7, 3));


// Funções Anônimas
let raiz = function (n) {
    return Math.sqrt(n);
};
console.log(raiz(9));
console.log(raiz(16));
console.log(raiz(25));

// Arrow Functions
// forma extensa
let subtracao = (x, y) => {
    return x - y;
};
console.log(subtracao(8, 2));

// forma simplificada
// só pode ser usada se existir apenas uma linha dentro e estivermos passando um parâmetro
let novaRaiz = n => n ** 0.5;
console.log(novaRaiz(16));




function
myFunction(
    a
) {
    a = a.toString();

    return a = a.split('')
}

console.log(myFunction(15));