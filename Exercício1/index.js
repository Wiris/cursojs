const primeiroNome = "Wiris Rafael";
const sobrenome = "Januario Wernek";
const idade = 19;
const peso = 80;
const altura = 1.7;
let imc = peso / (altura * altura);
let anoNascimento = 2021 - idade;

const imprimir = `${primeiroNome} ${sobrenome} tem ${idade} anos, nasceu em ${anoNascimento} pesa ${peso} kg, tem ${altura} de altura e seu IMC é ${imc.toFixed(
  2
)}`;

console.log(imprimir);
