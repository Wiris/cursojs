// Valores Primitivo(imutáveis) = string, number, boolean, undefined, null (bigint, symbol) - Valores Copiados
// São independentes
let a = "A";
let b = a;
console.log(a, b);
b = "B";
a = "C";
console.log(a, b);

// Referencia(mutável) = array, object, function - Valores passados por referência
let array1 = [1, 2, 3];
let array2 = array1;

console.log(array1, array2);
array1.push(4);
console.log(array1, array2);
array2.pop();
console.log(array1, array2);