// Operadores Aritméticos
// soma e concatenação (+)
// subtração (-)
// multiplicação (*)
// divisão (/)
// módulo (%)
// exponenciação (**)
// incremento (++)
// decremento (--)
// Ordem de Precedência 
// 1º (), 2º **, 3º[*, /, % ] 4º[ +, - ]

let n1 = 10;
let n2 = 5;
let n3 = '2';
console.log(n1 + n2);
console.log(n1 - n2);
console.log(n1 * n2);
console.log(n1 / n2);
console.log(n1 % n2);
console.log(n1 ** n2);
console.log(n1++);
console.log(n1);
console.log(++n1);
console.log(n1--);
console.log(n1);
console.log(--n1);

n1 += 1;
n1 -= 1;
n1 *= 1;
n1 /= 1;
n1 %= 1;
n1 **= 1;

n3 = parseInt(n3); // converte para inteiro
console.log(typeof n3);
n3 = parseFloat(n3); // converte para float
console.log(typeof n3);
n3 = Number(n3); // converte para número (inteiro ou float)
console.log(typeof n3);