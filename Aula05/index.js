// Constantes
// Constantes devem ter nomes significativos
// Nomes de constantes em javascript são case sensitive
// Constantes devem ser declaradas antes de serem usadas
// Não se pode utilizar palavras reservadas como constantes
// Não podem começar com números
// Não podem conter espaços ou traços
// Não se pode redeclarar constantes
// Utilizar const é uma boa prática
// Não se pode alterar o valor de uma constante
// Não se pode declarar constantes sem valor
// Utilizar o padrão camelCase
const nome = "João";
console.log(nome);
