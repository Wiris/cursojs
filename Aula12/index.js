let alunos = ["João", "Maria", "Pedro"];
console.log(alunos);
console.log(alunos[0]);
console.log(alunos[1]);
console.log(alunos[2]);


alunos[0] = "Felipe";
console.log(alunos);
alunos[3] = "Luiza";
console.log(alunos);

console.log(alunos.length); // tamanho do array
alunos.push("Marcos"); // adiciona um elemento no final do array
console.log(alunos);

alunos.unshift("Mariana"); // adiciona um elemento no inicio do array
console.log(alunos);

alunos.pop(); //remove o ultimo elemento do array
console.log(alunos);

alunos.shift(); //remove o primeiro elemento do array
console.log(alunos);

delete alunos[1]; //remove o elemento do array na posição passada sem alterar os indices
console.log(alunos);

console.log(alunos.slice(0, -2)); // retorna somente o intervalo de valores passado, o ultimo elemento é excluído

console.log(typeof alunos); // retorna o tipo de uma variavel
console.log(alunos instanceof Array); //verifica se é uma instância do objeto passado