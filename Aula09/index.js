// Utiliza-se \ como caracter de escape
let umaString = 'Oi eu sou uma string';
console.log(umaString.charAt(1)); // Mostra o caracter na posição passada
console.log(umaString.concat(' e eu também')); // Concatena duas ou mais strings
console.log(umaString.indexOf('uma')); // Mostra a posição do caracter passado
console.log(umaString.lastIndexOf('uma')); // Mostra a posição do caracter passado do final para o início
console.log(umaString.match(/[a-z]/)); // Busca por caracteres na string e retorna um array com os caracteres encontrados, a flag g faz com que a busca seja global e retorna um array com todos os caracteres encontrados
console.log(umaString.search(/[a-z]/)); // Busca por caracteres na string e retorna a posição do primeiro caracter encontrado, a flag g faz com que a busca seja global e retorna um array com todas as posições encontradas
console.log(umaString.replace('uma', 'uma string')); // Substitui uma string por outra,a flag g faz com que a busca seja global e substitui todas as ocorrências
console.log(umaString.length); // Mostra o tamanho da string
console.log(umaString.slice(15, 21)); // Corta a string a partir da posição passada e até a posição passada, não inclui a ultima posição
console.log(umaString.slice(-6)); // Corta a string a partir do tamananho menos o valor passado
console.log(umaString.split(' ')); // Divide a string em um array a partir do caracter passado
console.log(umaString.split(' ', 2)); // Divide a string em um array a partir do caracter passado, com o limite de elementos passado
console.log(umaString.toUpperCase()); // Converte a string para maiúsculo
console.log(umaString.toLowerCase()); // Converte a string para minúsculo