let pessoa1 = {
    nome: "João",
    sobrenome: "Ferreira",
    idade: 25
};

console.log(pessoa1.nome);
console.log(pessoa1.sobrenome);
console.log(pessoa1.idade);


function criaPessoa(nome, sobrenome, idade) {
    return {
        // forma estensa
        // nome: nome,
        // sobrenome: sobrenome,
        // idade: idade

        // forma simplifica
        nome,
        sobrenome,
        idade,

        // métodos
        fala() {
            console.log(`${this.nome} ${this.sobrenome} está falando oi`);
        },
        incrementarIdade() {
            ++this.idade;
        }
    }
}

let pessoa2 = criaPessoa("Luiz", "Augusto", 32);
console.log(pessoa2.nome);
console.log(pessoa2.sobrenome);
console.log(pessoa2.idade);
pessoa2.fala();
pessoa2.incrementarIdade();
console.log(pessoa2.idade);
pessoa2.fala();