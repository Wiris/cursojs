// Variáveis
// Variáveis devem ter nomes significativos
// Nomes de variáveis em javascript são case sensitive
// Variáveis devem ser declaradas antes de serem usadas
// Não se pode utilizar palavras reservadas como variáveis
// Não podem começar com números
// Não podem conter espaços ou traços
// Não se pode redeclarar variáveis com let
// Utilizar let é uma boa prática
// Utilizar o padrão camelCase
let nome = "João";
var nome1 = "Maria";

console.log(nome, "nasceu em 1984");
