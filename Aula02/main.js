// Aspas simples para usar aspas duplas no texto.
// EX: console.log('Wiris "Rafael"');
// Aspas duplas para usar aspas simples no texto.
// EX: console.log("'Wiris' Rafael");
// Crase para usar tanto aspas simples quanto duplas no texto.
// EX: console.log(`'Wiris` "Rafael"`);
console.log("Wiris Rafael");
console.log(`Wiris Rafael`);
console.log(20);
console.log(25, 15.85, "wiris");
